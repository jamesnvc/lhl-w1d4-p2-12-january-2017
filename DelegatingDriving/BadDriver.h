//
//  BadDriver.h
//  DelegatingDriving
//
//  Created by James Cash on 12-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"

@interface BadDriver : NSObject <Driver>

@end
