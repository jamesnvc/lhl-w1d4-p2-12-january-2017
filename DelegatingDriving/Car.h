//
//  Car.h
//  DelegatingDriving
//
//  Created by James Cash on 12-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Driver
- (BOOL)canHandleTheirShit;
//- (NSInteger)howFastToGo; /// Try doing this?
@end

@interface Car : NSObject

@property (nonatomic,weak) id<Driver> delegate;

- (void)drive;

@end
