//
//  Car.m
//  DelegatingDriving
//
//  Created by James Cash on 12-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Car.h"

@implementation Car

- (void)drive
{
    if (self.delegate == nil) {
        NSLog(@"Can't go anywhere without a driver");
        return;
    }
    if ([self.delegate canHandleTheirShit]) {
        NSLog(@"Had a good drive");
    } else {
        NSLog(@"🔥💩");
    }
}

@end
