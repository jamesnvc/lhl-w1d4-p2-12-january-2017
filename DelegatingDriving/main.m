//
//  main.m
//  DelegatingDriving
//
//  Created by James Cash on 12-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"
#import "GoodDriver.h"
#import "BadDriver.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        GoodDriver *goodDriver = [[GoodDriver alloc] init];
        BadDriver *badDriver = [[BadDriver alloc] init];
        Car *car = [[Car alloc] init];

//        car.delegate = goodDriver;
        car.delegate = badDriver;
        [car drive];

    }
    return 0;
}
